import nltk
import codecs
from string import punctuation
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np

#Abriendo y formateando texto
text = codecs.open("crimenYcastigo.txt", 'r', 'utf-8-sig')
#text = open("crimenYcastigo.txt",'r') Usar esta linea mantiene caracteres de la codificación
words = text.read()
text.close()

#Uso de nltk
tokens = nltk.word_tokenize(words)
print(tokens[:10])

#Se puede ver que existe texto del proveedor (The Project Gutenberg) que no entra al analisis, por lo que hay que remover esto
posStart = words.find("PART I")
posFinal = words.rfind("End of Project")
words = words[posStart:posFinal]
tokens = nltk.word_tokenize(words)
print(tokens[:10])

#Lemmatizar palabras
wnl = nltk.stem.WordNetLemmatizer()
for i in range(len(tokens)):
    tokens[i] = wnl.lemmatize(tokens[i])

#POS-tagger
tupleWordTag = nltk.pos_tag(tokens)

#Para contar sustantivos y verbos
nouns = ['NN','NNS','NNP','NNPS']
verbs = ['VB','VBD','VBG','VBN','VBP','VBZ']
numNouns = 0
numVerbs = 0
for pair in tupleWordTag:
    if (pair[1] in nouns):
        numNouns+= 1
    elif (pair[1] in verbs):
        numVerbs+=1
print("Number of nouns: %d" %numNouns)
print("Number of verbs: %d" %numVerbs)

#Eliminar signos de puntuacion
punctuationSigns = list(punctuation)
punctuationSigns.extend(["...","’","“","”","...","--"])
words = [t for t in tokens if t not in punctuationSigns]

#Encontrar palabras mas frecuentes
allWordDist = nltk.FreqDist(w.lower() for w in words)

stopwords = nltk.corpus.stopwords.words('english')
allWordExceptStopDist = nltk.FreqDist(w.lower() for w in words if w not in stopwords)

print("Common words(with wordstops)")
print(allWordDist.most_common(30))
elements = [word[0] for word in allWordDist.most_common(30)]
values = [word[1] for word in allWordDist.most_common(30)]
y_pos = np.arange(len(elements))
plt.barh(y_pos, values, align='center', alpha=0.5)
plt.yticks(y_pos, elements)
plt.xlabel('Frequency')
plt.title('Words')
plt.show()

print("Common words(with no wordstops)")
print(allWordExceptStopDist.most_common(30))
elements = [word[0] for word in allWordExceptStopDist.most_common(30)]
values = [word[1] for word in allWordExceptStopDist.most_common(30)]
y_pos = np.arange(len(elements))
plt.barh(y_pos, values, align='center', alpha=0.5)
plt.yticks(y_pos, elements)
plt.xlabel('Frequency')
plt.title('Words')
plt.show()

