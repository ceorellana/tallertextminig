import tweepy
import nltk
from nltk.collocations import *
from string import punctuation
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import csv
import networkx as nx

def checkNumber(op,min,max):
    if (op.isdigit()):
        op = int(op)
        if(op>min and op <max):
            return True
    return False

def graphStatistics(distribution):
    elements = [word[0] for word in distribution.most_common(30)]
    values = [word[1] for word in distribution.most_common(30)]
    y_pos = np.arange(len(elements))
    plt.barh(y_pos, values, align='center', alpha=0.5)
    plt.yticks(y_pos, elements)
    plt.xlabel('Frequency')
    plt.title('Words')
    plt.show()

def getTweets(account,api):
    try:
        tweets = api.user_timeline(screen_name=account, count=100, tweet_mode="extended")
        archivo = open("tweets.txt", "wb")
        for tweet in tweets:
            text = str(tweet.full_text)
            archivo.write((text + "\n").encode())
        archivo.close()
        print("Tweets retrieved from username %s"%account)
    except tweepy.TweepError:
        print("Username or connection error, please try again later or check if the username is correct")

def generateGraph():
    word_net = nx.read_edgelist('edges.csv', delimiter=",",data=(('weight',int),))  # Grafo dirigido
    #word_net = nx.read_weighted_edgelist('edges.csv', delimiter=",",encoding="utf-8")  # Grafo dirigido
    N = len(word_net)
    K = word_net.number_of_edges()

    print("Nodes ", N)
    print("Edges", K)

    #Convertir grafo a no dirigido
    word_net_ud = word_net.to_undirected()
    word_net_mc = max(nx.connected_component_subgraphs(word_net_ud), key=len)
    # Encontrar numero de nodos y aristas del GIANT
    N_mc, K_mc = len(word_net_mc), word_net_mc.number_of_edges()

    print("Giant component")
    print("Nodes ", N_mc)
    print("Edges", K_mc)

    pos = nx.draw_spring(word_net_mc)
    nx.draw(word_net_ud,pos,with_labels=True)
    plt.show()

def correlationGraph():
    try:
        # Abriendo y formateando texto
        text = open("tweets.txt", 'rb')
        words = text.read()
        text.close()

        # Insertar tamaño de ventana y frecuencia minima

        # windowSize indica la distancia maxima que puede estar una palabra cercana a otra para considerarse correlacionada
        windowSize = input("Write the size of the correlation window (min 3, max 7): ")
        while (checkNumber(windowSize, 2, 8) != True):
            windowSize = input("Write a valid number: ")
        windowSize = int(windowSize)

        # minFrequency indica la frecuencia minima que se aceptará entre dos palabras para guardar en el csv
        minFrequency = input("Write minimun frequency for a significant correlation (min 1, max dependant on user): ")
        while (checkNumber(minFrequency, 0, 999) != True):
            minFrequency = input("Write a valid number: ")
        minFrequency = int(minFrequency)

        # Uso de nltk
        tokens = nltk.word_tokenize(words.decode())

        # Lemmatizar palabras
        wnl = nltk.stem.WordNetLemmatizer()
        for i in range(len(tokens)):
            tokens[i] = wnl.lemmatize(tokens[i])

        # Eliminar signos de puntuacion
        punctuationSigns = list(punctuation)
        punctuationSigns.extend(["...", "’", "“", "”", "...", "--", "``", "''"])
        stopwords = nltk.corpus.stopwords.words('english')
        words = [t for t in tokens if t not in punctuationSigns and t not in stopwords]

        # Obtener correlacion entre palabras
        correlation = BigramCollocationFinder.from_words(words,window_size=windowSize)
        correlation.apply_freq_filter(minFrequency)
        bigram_measures = nltk.collocations.BigramAssocMeasures()
        correlation.score_ngrams(bigram_measures.pmi)

        # Creando csv para grafo
        csvFile = open("edges.csv","w",encoding="utf-8-sig",newline='')
        fieldnames = ["source","target","weight"]
        writer = csv.DictWriter(csvFile, fieldnames=fieldnames)

        writer.writeheader()
        for words,frequency in correlation.ngram_fd.items():
            writer.writerow({"source": words[0],"target": words[1],"weight": frequency})

        csvFile.close()
        print("File generated, returning to main menu...")
        generateGraph()

    except FileNotFoundError:
        print("File not found, please choice option 1 first to load tweets from a Twitter account")

def docsStatistics():
    try:
        # Abriendo y formateando texto
        text = open("tweets.txt", 'rb')
        words = text.read()
        text.close()

        # Uso de nltk
        tokens = nltk.word_tokenize(words.decode())

        # Lemmatizar palabras
        wnl = nltk.stem.WordNetLemmatizer()
        for i in range(len(tokens)):
            tokens[i] = wnl.lemmatize(tokens[i])

        # Eliminar signos de puntuacion
        punctuationSigns = list(punctuation)
        punctuationSigns.extend(["...", "’", "“", "”", "...", "--","``","''"])
        words = [t for t in tokens if t not in punctuationSigns]

        # Encontrar palabras mas frecuentes
        allWordDist = nltk.FreqDist(w.lower() for w in words)

        stopwords = nltk.corpus.stopwords.words('english')
        allWordExceptStopDist = nltk.FreqDist(w.lower() for w in words if w not in stopwords)

        print("Common words(with wordstops)")
        print(allWordDist.most_common(30))
        graphStatistics(allWordDist)

        print("Common words(with no wordstops)")
        print(allWordExceptStopDist.most_common(30))
        graphStatistics(allWordExceptStopDist)

    except FileNotFoundError:
        print("File not found, please choice option 1 first to load tweets from a Twitter account")

# Print menu function
def printMenu():
    print("--- \tTimeline analyzer\t ---")
    print("1. Change user\n"
          "2. Get words statistics\n"
          "3. Generate csv file and correlation graph\n"
          "4. Generate correlation graph\n"
          "5. Exit")

#Programa principal
consumer_key = "Wd4YIBALVcoenf8cc97AygpYd"
consumer_secret = "ZNH13uBH2us4PqDbXeRDElyPbDa3Fv40m4r3Bw3DuZGC7hSdRH"
access_token = "3019875448-rwec0sJOyQqDsYJgGuWrc4tCtQVi6Nr1HQGzAc7"
access_token_secret = "0h3blJQqrXBPKuvRAxiCj08HNxLeVgkKcWxGjKMKOlhEU"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

printMenu()
op = input("Select an option: ")
while(checkNumber(op,0,6)!=True):
    op = input("Select a valid option: ")
op = int(op)
while(op!=5):
    if(op == 1):
        user = input("Write a twitter's username (eg. @screenname): ")
        getTweets(user,api)
    elif(op == 2):
        docsStatistics()
    elif(op == 3):
        correlationGraph()
    elif(op == 4):
        generateGraph()
    printMenu()
    op = input("Select an option: ")
    while (checkNumber(op,0,6) != True):
        op = input("Select a valid option: ")
    op = int(op)
print("Closing program...")